package br.com.danielbastospereira;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLStreamHandler;

import javax.swing.text.Document;

import org.jsoup.Jsoup;
import org.silvertunnel_ng.netlib.adapter.java.JvmGlobalUtil;
import org.silvertunnel_ng.netlib.adapter.url.NetlibURLStreamHandlerFactory;
import org.silvertunnel_ng.netlib.api.NetFactory;
import org.silvertunnel_ng.netlib.api.NetLayer;
import org.silvertunnel_ng.netlib.api.NetLayerIDs;
import org.silvertunnel_ng.netlib.api.NetSocket;
import org.silvertunnel_ng.netlib.api.util.TcpipNetAddress;
import org.silvertunnel_ng.netlib.util.HttpUtil;

public class Teste {
	
	public void testar(){
		
		try {
		
//			JvmGlobalUtil.init();
//			NetLayer netLayer = NetFactory.getInstance().getNetLayerById(NetLayerIDs.TOR);
//			JvmGlobalUtil.setNetLayerAndNetAddressNameService(netLayer, true);
//			
//			String body = Jsoup.connect("http://www.google.com").execute().body();
//			
//			System.out.println( body );
			
//            NetLayer netLayerTor = NetFactory.getInstance().getNetLayerById(NetLayerIDs.TOR);
//            netLayerTor.waitUntilReady();
//            
//            NetlibURLStreamHandlerFactory factory = new NetlibURLStreamHandlerFactory(false);
//            factory.setNetLayerForHttpHttpsFtp(netLayerTor);
//            
//            
//            
//            String urlStr = "http://www.google.com/";
//            URLStreamHandler handler = factory.createURLStreamHandler("http");
//            URL context = null;
//            URL url = new URL(context, urlStr, handler);
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setDoOutput(true);
//            connection.setDoInput(true);
//            connection.setInstanceFollowRedirects(false); 
//            connection.setRequestMethod("GET"); 
//            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
//            connection.setRequestProperty("charset", "utf-8");
//            connection.setUseCaches(false);
//            
//            // Write data
//            OutputStream os = connection.getOutputStream();
//
//
//           // Read response
//           StringBuilder responseSB = new StringBuilder();
//            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//
//           String line;
//           while ( (line = br.readLine()) != null)
//              responseSB.append(line);
//
//           // Close streams
//           br.close();
//           os.close();
//           org.jsoup.nodes.Document doc = Jsoup.parse(responseSB.toString());
//           
//           System.out.println( doc.body() );

			NetLayer lowerNetLayer = NetFactory.getInstance().getNetLayerById(NetLayerIDs.TOR);

			// wait until TOR is ready (optional) - this is only relevant for anonymous communication:
			lowerNetLayer.waitUntilReady();

			// prepare parameters (http default port: 80)
			TcpipNetAddress httpServerNetAddress = new TcpipNetAddress("internacional.estadao.com.br", 80);
			String pathOnHttpServer = "/noticias/geral,pressionado-por-milicia-xiita-presidente-do-iemen-renuncia,1623377";
			long timeoutInMs = 5000;

			// do the HTTP request and wait for the response
			byte[] responseBody = HttpUtil.getInstance().get(lowerNetLayer, httpServerNetAddress, pathOnHttpServer, timeoutInMs);

			// print out the response
			org.jsoup.nodes.Document doc = Jsoup.parse(new String(responseBody));
			
			System.out.println( doc.html() );
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
    
	public static void main(String[] args) {
		
		Teste t = new Teste();
		t.testar();
		
	}
    
}
